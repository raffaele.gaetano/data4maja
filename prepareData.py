import gdal
import os
import getopt
import sys
import osr

def generateDTMConfig(base_fld):
    flds = os.listdir(base_fld)
    for fld in flds:
        tile = os.path.basename(fld).split('_T')[1][0:5]
        cf = open(tile + '.txt','w')

        bnd = ''
        found = False
        for r,d,f in os.walk('/'.join([base_fld,fld])):
            for n in f:
                if 'B04.jp2' in n:
                    bnd = r + '/' + n
                    found = True
                    break
            if found:
                break

        if bnd == '':
            sys.exit("Error")

        ds = gdal.Open(bnd)
        srs = osr.SpatialReference()
        srs.ImportFromWkt(ds.GetProjection())
        code = srs.GetAuthorityCode('PROJCS')
        aname = srs.GetAuthorityName('PROJCS')
        pname = 'UTM' + code[3:]
        if code[2] == '6':
            pname += 'N'
        else:
            pname += 'S'

        geoT = ds.GetGeoTransform()

        cf.write('proj='+pname+'\n')
        cf.write('EPSG_out=' + code + '\n')
        cf.write('chaine_proj=' + aname + ':' + code + '\n')
        cf.write('tx_min=0\n')
        cf.write('ty_min=0\n')
        cf.write('tx_max=0\n')
        cf.write('ty_max=0\n')
        cf.write('pas_x=109800\n')
        cf.write('pas_y=109800\n')
        cf.write('orig_x=' + str(int(geoT[0])) + '\n')
        cf.write('orig_y=' + str(int(geoT[3])) + '\n')
        cf.write('marge=0\n')

        cf.close()
        ds = None




if __name__ == "__main__":
    try:
        opts, args = getopt.getopt(sys.argv[1:], '', [])
    except getopt.GetoptError as err:
        sys.exit(err)

    if len(args) == 0 or not os.path.exists(args[0]):
        sys.exit('Provide a valid folder with L1C images.')

    generateDTMConfig(args[0])